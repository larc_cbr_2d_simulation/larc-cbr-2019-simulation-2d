# LARC/CBR 2019 #

## Robocup Soccer Simulation League 2D ##

The Simulation League uses a video display to run soccer games without the use of real robots. It resembles a video game. The match is played between autonomous systems rather than human players. Simulation League competitions are therefore sometimes called matches pitting AI against AI.

The game is performed on a server called a SoccerServer. Eleven players compete against eleven players. Using virtual sensors, each player receives inputs of data such as its surrounding situation and the position of the ball in order to act.

The Soccer 2D Simulation League is one of the oldest in RoboCupSoccer. The speedy soccer games competed by autonomous AI systems using sophisticated strategies are a must-see.

### Rio Grande - RS, Brazil [View in Google Maps](https://www.google.com/maps/place/Rio+Grande+-+RS/@-32.2079832,-52.9399031,9z)

This document contains the rules and procedures for the LARC/CBR2019 Soccer Simulation League 2D competition in Rio Grande – RS, Brazil

## Schedule

The detailed schedule will be presented at the competition site. [Click here](http://www.cbrobotica.org/?page_id=28)

## Score

**Winner:** ITAndroids

**Runner-up** RoboCIn

**3rd Place** Titans2019

## Players

[ITAndroids](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/itandroids-2019-10-26.tgz)

[RoboCIn](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/robocin-2019-10-26.tgz)

[Titans2019](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/titans-2019-10-25.tgz)

[RobotBulls](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/robotbulls-2019-10-25.tgz)

[FCP_GPR_2019](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/fcpgpr-2019-10-25.tgz)

[Futvasf2D](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/futvasf-2019-10-25.tgz)

[Spartron](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/spartron-2019-10-25.tgz)

[MuseSoccer](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/musesoccer-2019-10-25.tgz)

## Game Logs

[Scoreboard](https://docs.google.com/spreadsheets/d/1B9jmLI7aA-WQw7G4Jf15Vs-9ZEgbKqY9Kp8CgD9_tYg/edit?usp=sharing)

[Group A](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/group_A.tar.xz)

[Group B](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/group_B.tar.xz)

[Group C](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/group_C.tar.xz)

[Group D](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/group_D.tar.xz)

[3rd Place - Round robin](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/3rd_place.tar.xz) 

[3rd Place - Penalties](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/3rd_place_penalty.tar.xz)

[Final - Round Robin](https://bitbucket.org/larc_cbr_2d_simulation/larc-cbr-2019-simulation-2d/downloads/final.tar.xz)

## Operating System

Linux Ubuntu LTS 18.04 64bit will be used. [Click here](https://ubuntu.com/download/desktop)

## Soccer Simulator

Current  version  of  RCSSServer  version  is  15.5.0. [Click Here](https://github.com/rcsoccersim/rcssserver/releases) 

The  default  server  configuration  files  (server.conf  and player.conf)  generated  by  RCSSServer  15.5.0  will  be  used. [Click here](https://bitbucket.org/larc_cbr_2d_simulation/larc2017/wiki/Home) to get more information.

## General Tournament Rules

Note  that  the  number  of  turns, groups and teams depends of competitor’s number. 

* All matches will be started automatically by the league manager script. 
* Teams are only allowed to update their binaries by 1 hour before the first game starts in a round. Each team need to provide proper start, kill and team.yml scripts. Sample scripts will be in the top of your home directory (e.g. /home/helios/start).
* You MUST to provide binary name the same as team name. Default names like sample_player and sample_coach won't be allowed. 
* In order to test the automatic running games script (the league manager), teams have to finish testing their binaries and scripts in a competition machine at the most 1 hour before competition being started.
* Script will be executed by a different user in your user group. Your scripts and your binaries must to be able reading and running permission (chmod +x -R 755 *). 
* Double check if your killer scripts are killing all of your programs (goalie, players, and coach) even if your programs are finished automatically. If the scripts do not work properly, the organizers will not fix them. 
* Do NOT output too many data to stdout, stderr and files in your home directory because it might cause a serious network delay. 
* Do NOT change the team name during the tournament. The team name that is used in the first match must be unchanged until eld of championcship.

Violating this rules may cause non-qualification in the following LARC/CBR competitions. The home directory of each team will be published automatically after the competition ends.


### A. Tournament Score
In the group rounds, there will be 3 (three) points allocated for a win and 1 (one) point for a draw. After Group matches, playoff can be done in matches based on the results of the round-trip or two-way aggregated matches or by one match followed two extra times. If at the final of round the scoreboard is the same, penalties will be executed automatically. 

The team that does not present the binary until the start of the round will not score points and the result of 3:0 will be awarded to the opposing team. 

### B. Tiebreakers

Tiebreakers between more than one teams in the Group rounds will apply in the following priorities

1. Points;
2. Average goal difference for the round;
3. Average number of goals scored;
4. If there are more than two teams in a tie, overall number of goals scored including only games with the tied teams;
5. Penalty shootouts procedure among the tied teams.

### C. Automatic Penalty Shootouts Procedure 
* To  resolve  tie breaks  in  the  group  rounds  and  the  playoff  matches,  penalty  shootouts  will  be  used.  For penalty shootouts, we are going to use these parameters:
* *pen_dist_x: 42.5 (The ball will be placed 42.5m from the goal) 
* pen_allow_mult_kicks: true (allow multiple kicks so normal play) 
* pen_taken_wait: 200 (the number of cycles waited after start pen is 200)* 
(This means that the kicker starts 42.5 meters from the goal and can use multi turns and kicks. After at most 200 cycles, the shootout is stopped for the kicker).
We are going to use these parameters in the championship: 
* *nr_extra_halfs: 0 (no extra halves or matches)* 
(This means that teams cannot use heterogeneous players.)

## Code of Honor 

### A. Coach Messages 
The coach can issue arbitrary “freeform” messages except on play-on mode. The coach can send one advice, one info, and one definition, every 30 seconds – the rest will be ignored by the server. Therefore, the coach should not send more than three of those standard language directives per 30 seconds, so as not to flood the network.

### B. Fouls 
Free kicks and kick-ins are detected automatically by the soccer server in many relevant cases. Sometimes, however, fouls occur which can only be detected by the human referee who has to award a free kick to the disadvantaged team. Possible reasons to call a foul are:

* If one team surrounds the ball so that the other team cannot kick 
* If the goal is blocked by so many players so that the ball could not go in (rough guideline: a wall of players blocking the goal); 
* If a team intentionally blocks the movement of opponent players; 
* The number of goalie moves is limited to 2. It is possible to get around this by doing a small kick and catching again. This is allowed once then the referee is required to drop the ball on the closest corner of the penalty box (notice this practice is not encouraged we are just acknowledging the potential for miss - kicks 
– continual use may be considered violating the fair play commitment); 
* Anything else that appears to  violate  the  fair  play  commitment  may  also  be  called  as  a  foul  after consultation with the committee.

## Fair Play 

The goal of the game is play soccer according to fair and common sense understanding of soccer and to the  restrictions  imposed  by  the  virtual  simulated  world  of  the  soccer  server.  Circumvention  of  these restrictions is considered violating the fair play commitment and its use during the tournament games is strictly forbidden. Example: 

* Using another teams binaries in your team;
* Jamming the simulator by sending more than 3 or 4 commands per client per cycle;
* Using direct inter-process communication; 
* To  disturb  other  teams  communication  by  recording  and  sending  fake  communication  of  the opponent team. 

Any of the above is strictly forbidden
. 
Other strategies might be found violating the fair play commitment, 
after consultation among the committee. However, we expect it to be pretty clear what a fair team should 
l
ook like. In particular, the destructive disruption of opponent agent operation or the gain of advantage 
by other means than explicitly offered by the soccer server count as not fair play. If you are in doubt of 
using a certain method, please ask the commi
ttee before the tournament starts. If a team is found to use 
unfair programming methods during the tournament, it will be 
immediately disqualified
.
If a team is under suspicion of violating the fair
-
play agreement, the committee has the right to ask for 
source code inspection.